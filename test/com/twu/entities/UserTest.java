package com.twu.entities;

import org.junit.Test;

import static com.twu.entities.User.administrator;
import static com.twu.entities.User.borrower;
import static org.junit.Assert.*;

public class UserTest {
  @Test
  public void shouldReturnUserLibraryID() {
    User user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    String expected = "123-4567";
    assertEquals(expected, user.memberID());
  }

  @Test
  public void shouldReturnTrueWhenUserIsAdmin() {
    User user = administrator("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    assertTrue(user.isAdmin());
  }

  @Test
  public void shouldReturnFalseWhenUserIsNotAdmin() {
    User user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    assertFalse(user.isAdmin());
  }

  @Test
  public void shouldReturnTrueWhenPasswordsMatch() {
    User user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    assertTrue(user.authenticate("123-4567", "password123"));
  }

  @Test
  public void shouldReturnFalseOnPasswordMisMatch() {
    User user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    assertFalse(user.authenticate("123-4567", "password1234"));
  }

}
