package com.twu.entities;

import org.junit.Assert;
import org.junit.Test;

import java.util.ResourceBundle;

public class MovieTest {
  @Test
  public void shouldReturnMovieDetails() {
    Movie movie = new Movie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    String expected = formattedMovie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    Assert.assertEquals(expected, movie.details());
  }

  public String formattedMovie(int id, String movieName, int releaseYear, String directorName, String rating) {
    ResourceBundle bundle = ResourceBundle.getBundle("TableHeading");
    String format = bundle.getString("movieFormat");
    String details = String.format(format, id, movieName, releaseYear, directorName, rating);
    return details.toString();
  }
}
