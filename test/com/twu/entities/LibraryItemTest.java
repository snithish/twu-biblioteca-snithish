package com.twu.entities;

import org.junit.Test;

import static com.twu.entities.LibraryItem.Category;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LibraryItemTest {
  @Test
  public void shouldReturnTrueWhenCategoryMatchForBooks() {
    LibraryItem item = new Book(1010, "A Tale of Two Cities", "Charles Dickens", 1859);
    assertTrue(item.isOfCategory(Category.BOOK));
  }

  @Test
  public void shouldReturnFalseWhenCategoryMatchForBooks() {
    LibraryItem item = new Book(1010, "A Tale of Two Cities", "Charles Dickens", 1859);
    assertFalse(item.isOfCategory(Category.MOVIE));
  }

  @Test
  public void shouldReturnTrueWhenCategoryMatchForMovies() {
    LibraryItem item = new Movie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    assertTrue(item.isOfCategory(Category.MOVIE));
  }

  @Test
  public void shouldReturnFalseWhenCategoryMatchForMovies() {
    LibraryItem item = new Movie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    assertFalse(item.isOfCategory(Category.BOOK));
  }

  @Test
  public void shouldReturnTrueWhenUniqueIDMatches() {
    LibraryItem item = new Book(1010, "A Tale of Two Cities", "Charles Dickens", 1859);
    assertTrue(item.hasID(1010));
  }

  @Test
  public void shouldReturnFalseWhenUniqueIDMatches() {
    LibraryItem item = new Book(1010, "A Tale of Two Cities", "Charles Dickens", 1859);
    assertFalse(item.hasID(1020));
  }
}
