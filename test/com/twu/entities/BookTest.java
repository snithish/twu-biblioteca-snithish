package com.twu.entities;

import org.junit.Test;

import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

public class BookTest {
  public static String formattedBook(int id, String bookName, String authorName, int publishedYear) {
    ResourceBundle bundle = ResourceBundle.getBundle("TableHeading");
    String format = bundle.getString("bookFormat");
    return String.format(format, id, bookName, authorName, publishedYear);
  }

  @Test
  public void shouldReturnBookDetails() {
    Book bookOne = new Book(1010, "A Tale of Two Cities", "Charles Dickens", 1859);
    StringBuilder expected = new StringBuilder();
    expected.append(formattedBook(1010, "A Tale of Two Cities", "Charles Dickens", 1859));
    assertEquals(expected.toString(), bookOne.details());
  }
}
