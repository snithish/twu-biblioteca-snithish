package com.twu.menus;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.twu.biblioteca.*;
import com.twu.entities.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;

import java.util.ResourceBundle;

import static com.twu.entities.BookTest.formattedBook;
import static com.twu.entities.LibraryItem.Category.BOOK;
import static com.twu.entities.LibraryItem.Category.MOVIE;
import static com.twu.entities.User.borrower;
import static com.twu.menus.MenuOperation.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MenuOperationsTest {
  Library library;
  User user;

  @Before
  public void setUp() {
    Book bookOne = new Book(1010, "A Tale of Two Cities", "Charles Dickens", 1859);
    Book bookTwo = new Book(1020, "Alice's Adventures in Wonderland", "Charles Lutwidge Dodgson", 1865);
    Book bookThree = new Book(1030, "The Great Gatsby", "F. Scott Fitzgerald", 1925);
    InventoryItem bookOneInventory = new InventoryItem(bookOne, Library.Status.AVAILABLE);
    InventoryItem bookTwoInventory = new InventoryItem(bookTwo, Library.Status.AVAILABLE);
    InventoryItem bookThreeInventory = new InventoryItem(bookThree, Library.Status.AVAILABLE);
    MongoDB instance = MongoDB.instance();
    Datastore datastore = instance.getDatabase();
    datastore.save(bookOne);
    datastore.save(bookTwo);
    datastore.save(bookThree);
    datastore.save(bookOneInventory);
    datastore.save(bookTwoInventory);
    datastore.save(bookThreeInventory);
    Movie movieOne = new Movie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    Movie movieTwo = new Movie(3020, "Interstellar", 2015, "Christopher Nolan", "9.0");
    InventoryItem movieOneInventory = new InventoryItem(movieOne, Library.Status.AVAILABLE);
    InventoryItem movieTwoInventory = new InventoryItem(movieTwo, Library.Status.AVAILABLE);
    datastore.save(movieOne);
    datastore.save(movieTwo);
    datastore.save(movieOneInventory);
    user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    datastore.save(user);
    datastore.save(movieTwoInventory);
    library = new Library(instance);
  }

  @Test
  public void shouldReturnListOfBooks() {
    StringBuilder expected = new StringBuilder();
    ResourceBundle bundle = ResourceBundle.getBundle("TableHeading");
    String headingFormat = bundle.getString("bookFormat");
    String headingData = bundle.getString("bookHeading");
    String[] split = headingData.split(",");
    String heading = String.format(headingFormat, split);
    expected.append(heading);
    expected.append(formattedBook(1010, "A Tale of Two Cities", "Charles Dickens", 1859));
    expected.append(formattedBook(1020, "Alice's Adventures in Wonderland", "Charles Lutwidge Dodgson", 1865));
    expected.append(formattedBook(1030, "The Great Gatsby", "F. Scott Fitzgerald", 1925));
    MenuOperation listBook = new ListItems(BOOK);
    String result = listBook.execute(library);
    assertEquals(expected.toString(), result);
  }

  @Test
  public void shouldReturnSuccessMessageForValidBookCheckOut() {
    Reader reader = mock(Reader.class);
    when(reader.getItemID()).thenReturn(1010);
    MenuOperation bookCheckOut = new CheckOutItem(reader, BOOK, user);
    String expected = checkout("bookCheckOutSuccess");
    assertEquals(expected, bookCheckOut.execute(library));
  }

  @Test
  public void shouldReturnSuccessMessageForValidReturn() {
    Reader reader = mock(Reader.class);
    when(reader.getItemID()).thenReturn(1010);
    new CheckOutItem(reader, BOOK, user).execute(library);
    MenuOperation returnBook = new ReturnItem(reader, BOOK);
    String expected = returnTransaction("bookReturnSuccess");
    assertEquals(expected, returnBook.execute(library));
  }

  @Test
  public void shouldReturnListOfMovies() {
    library.checkOutItem(3020, MOVIE, user);
    ResourceBundle bundle = ResourceBundle.getBundle("TableHeading");
    String headingFormat = bundle.getString("movieFormat");
    String headingData = bundle.getString("movieHeading");
    String[] split = headingData.split(",");
    String heading = String.format(headingFormat, split);
    String expected = heading;
    expected += new MovieTest().formattedMovie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    MenuOperation listMovies = new ListItems(MOVIE);
    assertEquals(expected.toString(), listMovies.execute(library));
  }

  @Test
  public void shouldReturnSuccessOnValidMovieCheckOut() {
    library.checkOutItem(3020, MOVIE, user);
    Reader reader = mock(Reader.class);
    when(reader.getItemID()).thenReturn(3010);
    MenuOperation movieCheckOut = new CheckOutItem(reader, MOVIE, user);
    String expected = checkout("movieCheckOutSuccess");
    assertEquals(expected.toString(), movieCheckOut.execute(library));
  }

  @Test
  public void shouldReturnSuccessOnValidMovieReturn() {
    library.checkOutItem(3020, MOVIE, user);
    Reader reader = mock(Reader.class);
    when(reader.getItemID()).thenReturn(3010);
    new CheckOutItem(reader, MOVIE, user).execute(library);
    MenuOperation returnBook = new ReturnItem(reader, MOVIE);
    String expected = returnTransaction("movieReturnSuccess");
    assertEquals(expected.toString(), returnBook.execute(library));
  }

  @Test
  public void shouldReturnNewLineInCheckOutItemWhenInputIsBack() {
    Reader reader = mock(Reader.class);
    when(reader.getItemID()).thenReturn(-1);
    String result = new CheckOutItem(reader, MOVIE, user).execute(library);
    assertEquals("\n", result);
  }

  @Test
  public void shouldReturnNewLineInReturnItemWhenInputIsBack() {
    Reader reader = mock(Reader.class);
    when(reader.getItemID()).thenReturn(-1);
    String result = new ReturnItem(reader, MOVIE).execute(library);
    assertEquals("\n", result);
  }

  @Test
  public void shouldReturnUserIDWhenAuthenticationIsSuccessful() throws AuthenticationFailedException {
    UserManagement userManagement = mock(UserManagement.class);
    Reader reader = mock(Reader.class);
    when(reader.getLibraryID()).thenReturn("123-4567");
    when(reader.getPassword()).thenReturn("password");
    when(userManagement.login("123-4567", "password")).thenReturn(user);
    LogIn login = new MenuOperation.LogIn(userManagement, reader);
    String result = login.execute(library);
    assertEquals("123-4567", result);
  }

  @Test
  public void shouldReturnAuthenticationFailedWhenAuthenticationIsNotSuccessful() throws AuthenticationFailedException {
    UserManagement userManagement = mock(UserManagement.class);
    Reader reader = mock(Reader.class);
    when(reader.getLibraryID()).thenReturn("123-4567");
    when(reader.getPassword()).thenReturn("password");
    when(userManagement.login("123-4567", "password")).thenThrow(AuthenticationFailedException.class);
    LogIn login = new MenuOperation.LogIn(userManagement, reader);
    String result = login.execute(library);
    assertEquals("Authentication Failed", result);
  }

  @Test
  public void shouldReturnLedgerDetails() {
    library.checkOutItem(1010, BOOK, user);
    LedgerEntry entry = new LedgerEntry("123-4567", "A Tale of Two Cities");
    CheckOutLedger ledger = new CheckOutLedger();
    String result = ledger.execute(library);
    assertEquals(entry.details(), result);
  }

  private String returnTransaction(String key) {
    return ResourceBundle.getBundle("LibraryReturn").getString(key);
  }

  private String checkout(String key) {
    return ResourceBundle.getBundle("LibraryCheckOut").getString(key);
  }

  @After
  public void cleanUp() {
    MongoClient mongoClient = new MongoClient();
    DB db = mongoClient.getDB("libraryTest");
    if (db.collectionExists("items")) {
      DBCollection myCollection = db.getCollection("items");
      myCollection.drop();
    }
    if (db.collectionExists("User")) {
      DBCollection myCollection = db.getCollection("User");
      myCollection.drop();
    }
    if (db.collectionExists("InventoryItem")) {
      DBCollection myCollection = db.getCollection("InventoryItem");
      myCollection.drop();
    }
  }
}
