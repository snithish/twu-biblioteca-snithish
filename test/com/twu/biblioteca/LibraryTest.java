package com.twu.biblioteca;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.twu.entities.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;

import java.util.List;

import static com.twu.biblioteca.Library.Status;
import static com.twu.biblioteca.LibraryResult.Status.FAILURE;
import static com.twu.biblioteca.LibraryResult.Status.SUCCESS;
import static com.twu.biblioteca.LibraryResult.TransactionType.CHECKOUT;
import static com.twu.biblioteca.LibraryResult.TransactionType.RETURN;
import static com.twu.entities.BookTest.formattedBook;
import static com.twu.entities.LibraryItem.Category.BOOK;
import static com.twu.entities.LibraryItem.Category.MOVIE;
import static com.twu.entities.User.borrower;
import static org.junit.Assert.assertEquals;

public class LibraryTest {
  Library library;
  User user;

  @Before
  public void setUp() {
    Book bookOne = new Book(1010, "A Tale of Two Cities", "Charles Dickens", 1859);
    Book bookTwo = new Book(1020, "Alice's Adventures in Wonderland", "Charles Lutwidge Dodgson", 1865);
    Book bookThree = new Book(1030, "The Great Gatsby", "F. Scott Fitzgerald", 1925);
    InventoryItem bookOneInventory = new InventoryItem(bookOne, Status.AVAILABLE);
    InventoryItem bookTwoInventory = new InventoryItem(bookTwo, Status.AVAILABLE);
    InventoryItem bookThreeInventory = new InventoryItem(bookThree, Status.AVAILABLE);
    MongoDB instance = MongoDB.instance();
    Datastore datastore = instance.getDatabase();
    datastore.save(bookOne);
    datastore.save(bookTwo);
    datastore.save(bookThree);
    datastore.save(bookOneInventory);
    datastore.save(bookTwoInventory);
    datastore.save(bookThreeInventory);
    Movie movieOne = new Movie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    Movie movieTwo = new Movie(3020, "Interstellar", 2015, "Christopher Nolan", "9.0");
    InventoryItem movieOneInventory = new InventoryItem(movieOne, Status.AVAILABLE);
    InventoryItem movieTwoInventory = new InventoryItem(movieTwo, Status.AVAILABLE);
    datastore.save(movieOne);
    datastore.save(movieTwo);
    datastore.save(movieOneInventory);
    user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    datastore.save(user);
    datastore.save(movieTwoInventory);
    library = new Library(instance);
  }

  @Test
  public void shouldReturnListOfBooksInLibrary() {

    StringBuilder expected = new StringBuilder();
    expected.append(formattedBook(1010, "A Tale of Two Cities", "Charles Dickens", 1859));
    expected.append(formattedBook(1020, "Alice's Adventures in Wonderland", "Charles Lutwidge Dodgson", 1865));
    expected.append(formattedBook(1030, "The Great Gatsby", "F. Scott Fitzgerald", 1925));
    StringBuilder result = iterateOverBooksAndReturnDetails(library);
    assertEquals(expected.toString(), result.toString());
  }

  @Test
  public void shouldReturnSuccessMessageOnSuccessfulCheckoutBook() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, SUCCESS, BOOK);
    assertEquals(libraryResult.detail(), library.checkOutItem(1020, BOOK, user).detail());
  }

  @Test
  public void shouldFailWhenTryingToCheckOutAlreadyCheckedOutBook() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, FAILURE, BOOK);
    library.checkOutItem(1010, BOOK, user);
    assertEquals(libraryResult.detail(), library.checkOutItem(1010, BOOK, user).detail());
  }

  @Test
  public void shouldAddBookToAvailableBooksWhenBookIsReturned() {
    library.checkOutItem(1010, BOOK, user);
    library.checkOutItem(1020, BOOK, user);
    library.checkOutItem(1030, BOOK, user);
    library.returnItem(1010, BOOK);
    StringBuilder expected = new StringBuilder();
    expected.append(formattedBook(1010, "A Tale of Two Cities", "Charles Dickens", 1859));
    StringBuilder result = iterateOverBooksAndReturnDetails(library);
    assertEquals(expected.toString(), result.toString());
  }

  @Test
  public void shouldFailWhenCheckOutOfNonExistentBook() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, FAILURE, BOOK);
    String result = library.checkOutItem(2000, BOOK, user).detail();
    assertEquals(libraryResult.detail(), result);
  }

  // Movie Test

  @Test
  public void shouldReturnSuccessMessageWhenBookReturnedBelongsToLibrary() {
    LibraryResult libraryResult = new LibraryResult(RETURN, SUCCESS, BOOK);
    library.checkOutItem(1020, BOOK, user);

    String result = library.returnItem(1020, BOOK).detail();
    assertEquals(libraryResult.detail(), result);
  }

  @Test
  public void shouldReturnFailureMessageWhenBookReturnedDoesNotBelongToLibrary() {
    LibraryResult libraryResult = new LibraryResult(RETURN, FAILURE, BOOK);
    String result = library.returnItem(2020, BOOK).detail();
    assertEquals(libraryResult.detail(), result);
  }

  @Test
  public void shouldReturnListOfMoviesInLibrary() {
    library.checkOutItem(3020, MOVIE, user);
    String expected = new MovieTest().formattedMovie(3010, "Inception", 2010, "Christopher Nolan", "8.8");
    StringBuilder result = iterateOverMoviesAndReturnDetails(library);
    assertEquals(expected.toString(), result.toString());
  }

  private StringBuilder iterateOverMoviesAndReturnDetails(Library library) {
    StringBuilder result = new StringBuilder();
    List<LibraryItem> items = library.listItems(MOVIE);
    for (LibraryItem item : items) {
      result.append(item.details());
    }
    return result;
  }

  @Test
  public void shouldReturnSuccessMessageOnSuccessfulCheckoutMovie() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, SUCCESS, MOVIE);
    String result = library.checkOutItem(3010, MOVIE, user).detail();
    assertEquals(libraryResult.detail(), result);
  }

  @Test
  public void shouldFailWhenCheckOutOfNonExistentMovie() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, FAILURE, MOVIE);
    String result = library.checkOutItem(2000, MOVIE, user).detail();
    assertEquals(libraryResult.detail(), result);
  }

  @Test
  public void shouldFailWhenTryingToCheckOutAlreadyCheckedOutMovie() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, FAILURE, MOVIE);
    library.checkOutItem(3010, MOVIE, user);
    String result = library.checkOutItem(3010, MOVIE, user).detail();
    assertEquals(libraryResult.detail(), result);
  }

  // CheckOut LedgerEntry

  @Test
  public void shouldReturnIDOfCustomerWhoCheckedOutBook() {
    library.checkOutItem(1010, BOOK, user);
    LedgerEntry entry = new LedgerEntry("123-4567", "A Tale of Two Cities");
    assertEquals(entry.details(), library.checkoutLedger(BOOK).get(0).details());
  }

  @Test
  public void shouldReturnEmptyStringWhenAllItemsAreReturned() {
    library.checkOutItem(1010, BOOK, user);
    library.returnItem(1010, BOOK);
    assertEquals(0, library.checkoutLedger(BOOK).size());
  }

  private StringBuilder iterateOverBooksAndReturnDetails(Library library) {
    StringBuilder result = new StringBuilder();
    List<LibraryItem> availableBooks = library.listItems(BOOK);
    for (LibraryItem availableBook : availableBooks) {
      result.append(availableBook.details());
    }
    return result;
  }

  @After
  public void cleanUp() {
    MongoClient mongoClient = new MongoClient();
    DB db = mongoClient.getDB("libraryTest");
    if (db.collectionExists("items")) {
      DBCollection myCollection = db.getCollection("items");
      myCollection.drop();
    }
    if (db.collectionExists("User")) {
      DBCollection myCollection = db.getCollection("User");
      myCollection.drop();
    }
    if (db.collectionExists("InventoryItem")) {
      DBCollection myCollection = db.getCollection("InventoryItem");
      myCollection.drop();
    }
  }
}
