package com.twu.biblioteca;

import com.twu.entities.User;
import org.junit.Assert;
import org.junit.Test;
import org.mongodb.morphia.Datastore;

import static com.twu.entities.User.administrator;
import static com.twu.entities.User.borrower;

public class UserManagementTest {
  @Test
  public void shouldReturnNotNullOnSuccessFulLogIn() throws AuthenticationFailedException {
    UserManagement userManagement = accountManager();
    Assert.assertNotNull(userManagement.login("123-4567", "password123"));
  }

  @Test(expected = AuthenticationFailedException.class)
  public void shouldReturnNullWhenLoginFails() throws AuthenticationFailedException {
    UserManagement userManagement = accountManager();
    userManagement.login("123-4567", "password");
  }

  @Test
  public void shouldReturnUserWithGivenID() throws UserNotFoundException {
    UserManagement userManagement = accountManager();
    User user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    Assert.assertEquals(user.memberID(), userManagement.getUser("123-4567").memberID());
  }

  @Test(expected = UserNotFoundException.class)
  public void shouldThrowExceptionWhenUserNotFoundWithGivenID() throws UserNotFoundException {
    UserManagement userManagement = accountManager();
    User user = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    Assert.assertEquals(user.memberID(), userManagement.getUser("123-2227").memberID());
  }

  private UserManagement accountManager() {
    User borrower = borrower("123-4567", "Some User", "su@su.com", "+91 8823456789", "password123");
    User admin = administrator("123-4589", "Some User", "su@su.com", "+91 8823456789", "password123");
    MongoDB instance = MongoDB.instance();
    Datastore datastore = instance.getDatabase();
    datastore.save(borrower);
    datastore.save(admin);
    return new UserManagement(instance);
  }
}
