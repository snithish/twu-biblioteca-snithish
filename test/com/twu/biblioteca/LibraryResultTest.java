package com.twu.biblioteca;

import org.junit.Test;

import java.util.ResourceBundle;

import static com.twu.biblioteca.LibraryResult.Status.FAILURE;
import static com.twu.biblioteca.LibraryResult.Status.SUCCESS;
import static com.twu.biblioteca.LibraryResult.TransactionType.CHECKOUT;
import static com.twu.biblioteca.LibraryResult.TransactionType.RETURN;
import static com.twu.entities.LibraryItem.Category.BOOK;
import static com.twu.entities.LibraryItem.Category.MOVIE;
import static org.junit.Assert.assertEquals;

public class LibraryResultTest {
  @Test
  public void shouldReturnCheckOutSuccessMessageForBook() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, SUCCESS, BOOK);
    String expected = checkout("bookCheckOutSuccess");
    assertEquals(expected, libraryResult.detail());
  }

  @Test
  public void shouldCheckOutSuccessMessageForMovie() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, SUCCESS, MOVIE);
    String expected = checkout("movieCheckOutSuccess");
    assertEquals(expected, libraryResult.detail());
  }

  @Test
  public void shouldReturnCheckOutFailureMessageForBook() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, FAILURE, BOOK);
    String expected = checkout("bookCheckOutFailure");
    assertEquals(expected, libraryResult.detail());
  }

  @Test
  public void shouldReturnCheckOutFailureMessageForMovie() {
    LibraryResult libraryResult = new LibraryResult(CHECKOUT, FAILURE, MOVIE);
    String expected = checkout("movieCheckOutFailure");
    assertEquals(expected, libraryResult.detail());
  }

  //Return Status
  @Test
  public void shouldReturnReturnSuccessMessageForBook() {
    LibraryResult libraryResult = new LibraryResult(RETURN, SUCCESS, BOOK);
    String expected = returnTransaction("bookReturnSuccess");
    assertEquals(expected, libraryResult.detail());
  }

  @Test
  public void shouldReturnReturnSuccessMessageForMovie() {
    LibraryResult libraryResult = new LibraryResult(RETURN, SUCCESS, MOVIE);
    String expected = returnTransaction("movieReturnSuccess");
    assertEquals(expected, libraryResult.detail());
  }

  @Test
  public void shouldReturnReturnFailureMessageForBook() {
    LibraryResult libraryResult = new LibraryResult(RETURN, FAILURE, BOOK);
    String expected = returnTransaction("bookReturnFailure");
    assertEquals(expected, libraryResult.detail());
  }

  @Test
  public void shouldReturnReturnFailureMessageForMovie() {
    LibraryResult libraryResult = new LibraryResult(RETURN, FAILURE, MOVIE);
    String expected = returnTransaction("movieReturnFailure");
    assertEquals(expected, libraryResult.detail());
  }

  private String checkout(String key) {
    return ResourceBundle.getBundle("LibraryCheckOut").getString(key);
  }

  private String returnTransaction(String key) {
    return ResourceBundle.getBundle("LibraryReturn").getString(key);
  }
}
