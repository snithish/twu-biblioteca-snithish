package com.twu.biblioteca;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LedgerEntryTest {
  @Test
  public void shouldReturnDetailsOfItemsInLedger() {
    LedgerEntry ledger = new LedgerEntry("123-4567", "A Tale of Two Cities");
    String expected = "User 123-4567 has checked out A Tale of Two Cities\n";
    assertEquals(expected, ledger.details());
  }
}
