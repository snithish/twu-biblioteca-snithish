package com.twu.biblioteca;

import org.junit.Test;

import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;

public class BibliotecaAppTest {
  @Test
  public void shouldReturnAGreeterMessage() {
    BibliotecaApp app = new BibliotecaApp();
    String expected = ResourceBundle.getBundle("BibliotecaApp").getString("Greeting");
    assertEquals(expected, app.greet());
  }
}
