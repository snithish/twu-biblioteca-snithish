package com.twu.biblioteca;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.twu.entities.LibraryItem;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * MongoDB providing the database connection for main.
 */
public class MongoDB {


  private static final Logger LOG = Logger.getLogger(MongoDB.class.getName());

  private static final MongoDB INSTANCE = new MongoDB();

  private final Datastore datastore;

  private MongoDB() {
    ResourceBundle bundle = ResourceBundle.getBundle("config");
    String db_host = bundle.getString("DB_HOST");
    String db_port_value = bundle.getString("DB_PORT");
    int db_port = Integer.parseInt(db_port_value);
    String db_name = bundle.getString("DB_NAME");
    MongoClientOptions mongoOptions = MongoClientOptions.builder().socketTimeout(60000).connectTimeout(15000)
        .maxConnectionIdleTime(600000).readPreference(ReadPreference.primaryPreferred()).build();
    MongoClient mongoClient;
    mongoClient = new MongoClient(new ServerAddress(db_host, db_port), mongoOptions);
    Morphia morphia = new Morphia().mapPackage(LibraryItem.class.getPackage().getName());
    datastore = morphia.createDatastore(mongoClient, db_name);
    datastore.ensureIndexes();
    datastore.ensureCaps();
    LOG.info("Connection to database '" + db_host + ":" + db_port + "/" + db_name + "' initialized");
  }

  public static MongoDB instance() {
    return INSTANCE;
  }

  public Datastore getDatabase() {
    return datastore;
  }
}