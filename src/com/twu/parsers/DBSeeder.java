package com.twu.parsers;

import com.twu.biblioteca.LibraryDataSourceAble;
import com.twu.biblioteca.MongoDB;
import com.twu.biblioteca.UserDataSourceAble;
import com.twu.entities.*;

import java.util.ArrayList;
import java.util.List;

import static com.twu.biblioteca.Library.Status.AVAILABLE;

public class DBSeeder {

  public static void main(String[] args) {
    MongoDB dbInstance = MongoDB.instance();
    DBSeeder seeder = new DBSeeder();
    LibraryDataSourceAble dataFetcher = new LibraryDataSource();
    UserDataSourceAble userDataFetcher = new UserDataSource();
    List<LibraryItem> items = seeder.library(dataFetcher);
    for (LibraryItem item : items) {
      dbInstance.getDatabase().save(item);
    }
    List<User> users = seeder.users(userDataFetcher);
    for (User user : users) {
      dbInstance.getDatabase().save(user);
    }
    for (LibraryItem item : items) {
      InventoryItem inventoryItem = new InventoryItem(item, AVAILABLE);
      dbInstance.getDatabase().save(inventoryItem);
    }
  }

  private List<LibraryItem> library(LibraryDataSourceAble dataFetcher) {
    ArrayList<LibraryItem> items = new ArrayList<LibraryItem>();
    List<Book> books = dataFetcher.listOfBooks();
    List<Movie> movies = dataFetcher.listOfMovies();
    for (Movie movie : movies) {
      items.add(movie);
    }
    for (Book book : books) {
      items.add(book);

    }
    return items;
  }

  private List<User> users(UserDataSourceAble dataFetcher) {
    List<User> users = dataFetcher.listOfUsers();
    return users;
  }

}
