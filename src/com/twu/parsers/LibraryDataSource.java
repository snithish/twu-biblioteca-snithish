package com.twu.parsers;

import com.twu.biblioteca.LibraryDataSourceAble;
import com.twu.entities.Book;
import com.twu.entities.Movie;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class LibraryDataSource implements LibraryDataSourceAble {
  static int itemCounter = 1000;
  ResourceBundle parserBundle;

  public LibraryDataSource() {
    this.parserBundle = ResourceBundle.getBundle("BibliotecaApp");
  }

  @Override
  public List<Book> listOfBooks() {
    List<Book> bookList = new ArrayList<Book>();
    try {
      NodeList books = getNodeList("BookSourcePath", "book");
      for (int counter = 0; counter < books.getLength(); counter++) {
        Node bookNode = books.item(counter);
        Element book = (Element) bookNode;
        String bookName = valueBasedOnTag(book, "name");
        String author = valueBasedOnTag(book, "author");
        String year = valueBasedOnTag(book, "year");
        int publishedYear = Integer.parseInt(year);
        Book aBook = new Book(itemCounter, bookName, author, publishedYear);
        bookList.add(aBook);
        itemCounter++;
      }
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return bookList;
  }

  @Override
  public List<Movie> listOfMovies() {
    List<Movie> movieList = new ArrayList<Movie>();
    try {
      NodeList movies = getNodeList("MovieSourcePath", "movie");
      for (int counter = 0; counter < movies.getLength(); counter++) {
        Node movieNode = movies.item(counter);
        Element movie = (Element) movieNode;
        String movieName = valueBasedOnTag(movie, "name");
        String year = valueBasedOnTag(movie, "year");
        String director = valueBasedOnTag(movie, "director");
        String rating = valueBasedOnTag(movie, "rating");
        int releaseYear = Integer.parseInt(year);
        Movie aMovie = new Movie(itemCounter, movieName, releaseYear, director, rating);
        movieList.add(aMovie);
        itemCounter++;
      }
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return movieList;
  }

  private NodeList getNodeList(String sourcePath, String tag) throws ParserConfigurationException, SAXException, IOException {
    ClassLoader loader = getClass().getClassLoader();
    String fileName = parserBundle.getString(sourcePath);
    File file = new File(loader.getResource(fileName).getFile());
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.parse(file);
    return document.getElementsByTagName(tag);
  }

  private String valueBasedOnTag(Element element, String key) {
    return element.getElementsByTagName(key).item(0).getTextContent().trim();
  }
}
