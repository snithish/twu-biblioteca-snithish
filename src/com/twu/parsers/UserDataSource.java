package com.twu.parsers;

import com.twu.biblioteca.UserDataSourceAble;
import com.twu.entities.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static com.twu.entities.User.administrator;
import static com.twu.entities.User.borrower;

public class UserDataSource implements UserDataSourceAble {
  ResourceBundle parserBundle;

  public UserDataSource() {
    this.parserBundle = ResourceBundle.getBundle("BibliotecaApp");
  }

  @Override
  public List<User> listOfUsers() {
    List<User> userList = new ArrayList<User>();
    try {
      NodeList users = getNodeList("UserSourcePath", "user");
      for (int counter = 0; counter < users.getLength(); counter++) {
        Node userNode = users.item(counter);
        Element user = (Element) userNode;
        String libraryID = valueBasedOnTag(user, "memberID");
        String type = valueBasedOnTag(user, "type");
        String userName = valueBasedOnTag(user, "name");
        String email = valueBasedOnTag(user, "email");
        String phone = valueBasedOnTag(user, "phone");
        String password = valueBasedOnTag(user, "password");
        if (type.equals("admin")) {
          User admin = administrator(libraryID, userName, email, phone, password);
          userList.add(admin);
        }
        if (type.equals("borrower")) {
          User borrower = borrower(libraryID, userName, email, phone, password);
          userList.add(borrower);
        }
      }
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return userList;
  }

  private NodeList getNodeList(String sourcePath, String tag) throws ParserConfigurationException, SAXException, IOException {
    ClassLoader loader = getClass().getClassLoader();
    String fileName = parserBundle.getString(sourcePath);
    File file = new File(loader.getResource(fileName).getFile());
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.parse(file);
    return document.getElementsByTagName(tag);
  }

  private String valueBasedOnTag(Element element, String key) {
    return element.getElementsByTagName(key).item(0).getTextContent().trim();
  }
}
