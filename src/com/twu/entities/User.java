package com.twu.entities;

//models a person using library

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity
public class User {
  @Id
  private String memberID;
  private String name;
  private String email;
  private String phoneNumber;
  private UserType type;
  private String password;


  public User() {

  }

  public User(String memberID, String name, String email, String phoneNumber, UserType type, String password) {

    this.memberID = memberID;
    this.name = name;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.type = type;
    this.password = password;
  }

  public static User administrator(String libraryID, String name, String email, String phoneNumber, String password) {
    return new User(libraryID, name, email, phoneNumber, UserType.ADMIN, password);
  }

  public static User borrower(String libraryID, String name, String email, String phoneNumber, String password) {
    return new User(libraryID, name, email, phoneNumber, UserType.BORROWER, password);
  }

  public String memberID() {
    return memberID;
  }

  public boolean isAdmin() {
    return type == UserType.ADMIN;
  }

  public boolean authenticate(String memberID, String password) {
    return this.memberID.equals(memberID) && this.password.equals(password);
  }

  public String details() {
    StringBuilder result = new StringBuilder();
    result.append("Name: ").append(name).append("\n");
    result.append("e-mail ID: ").append(email).append("\n");
    result.append("Phone Number: ").append(phoneNumber).append("\n");
    return result.toString();
  }

  private enum UserType {
    ADMIN, BORROWER
  }
}
