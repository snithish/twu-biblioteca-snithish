package com.twu.entities;

import org.mongodb.morphia.annotations.Entity;

import java.util.ResourceBundle;

// models a written work

@Entity(value = "items")
public class Book extends LibraryItem {
  private String bookName;
  private String authorName;
  private int publishedYear;

  public Book() {
    super();

  }

  public Book(int id, String bookName, String authorName, int publishedYear) {
    super(id, Category.BOOK);
    this.bookName = bookName;
    this.authorName = authorName;
    this.publishedYear = publishedYear;
  }

  @Override
  public String details() {
    return formattedOutput();
  }

  @Override
  public String name() {
    return bookName;
  }

  private String formattedOutput() {
    ResourceBundle bundle = ResourceBundle.getBundle("TableHeading");
    String format = bundle.getString("bookFormat");
    return String.format(format, super.uniqueID, bookName, authorName, publishedYear);
  }
}
