package com.twu.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import static com.twu.biblioteca.Library.Status;
import static com.twu.biblioteca.Library.Status.AVAILABLE;
import static com.twu.biblioteca.Library.Status.CHECKED_OUT;
import static com.twu.entities.LibraryItem.Category;

@Entity
public class InventoryItem {
  @Id
  private ObjectId id;
  @Reference
  private LibraryItem item;
  private Status status;
  @Reference
  private User user;

  public InventoryItem() {

  }

  public InventoryItem(LibraryItem item, Status status) {
    this.item = item;
    this.status = status;
    this.user = null;
  }

  public void checkOut(User libraryID) {
    user = libraryID;
    this.status = CHECKED_OUT;
  }

  public void returnItem() {
    this.status = AVAILABLE;
    user = null;
  }

  public boolean isOfCategory(Category category) {
    return item.isOfCategory(category);
  }

  public boolean isReturnable(int uid) {
    boolean isItemCheckedOut = status == CHECKED_OUT;
    return isItemCheckedOut && item.hasID(uid);
  }

  public boolean isAvailable() {
    return status == AVAILABLE;
  }

  public LibraryItem item() {
    return item;
  }

  public String userMemberID() {
    return user.memberID();
  }
}
