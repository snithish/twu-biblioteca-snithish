package com.twu.entities;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

//models any object that can be borrowed from library

@Entity(value = "items")
public abstract class LibraryItem {
  @Id
  protected int uniqueID;
  protected Category category;

  public LibraryItem(int id, Category category) {
    this.uniqueID = id;
    this.category = category;
  }

  public LibraryItem() {

  }

  public Boolean hasID(int uid) {
    return this.uniqueID == uid;
  }

  public Boolean isOfCategory(Category category) {
    return this.category == category;
  }

  abstract public String details();

  abstract public String name();

  public enum Category {
    BOOK, MOVIE
  }
}
