package com.twu.entities;

import org.mongodb.morphia.annotations.Entity;

import java.util.ResourceBundle;

//models a story or event captured on camera

@Entity(value = "items")
public class Movie extends LibraryItem {
  private String movieName;
  private int releaseYear;
  private String directorName;
  private String rating;

  public Movie() {

  }

  public Movie(int id, String movieName, int releaseYear, String directorName, String rating) {
    super(id, Category.MOVIE);

    this.movieName = movieName;
    this.releaseYear = releaseYear;
    this.directorName = directorName;
    this.rating = rating;
  }

  @Override
  public String details() {
    return formattedOutput();
  }

  @Override
  public String name() {
    return movieName;
  }

  private String formattedOutput() {
    ResourceBundle bundle = ResourceBundle.getBundle("TableHeading");
    String format = bundle.getString("movieFormat");
    String details = String.format(format, super.uniqueID, movieName, releaseYear, directorName, rating);
    return details.toString();
  }

}
