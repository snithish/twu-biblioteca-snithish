package com.twu.biblioteca;

import com.twu.menus.LoginMenu;
import io.console.ConsoleReader;
import io.console.ConsoleWriter;

import java.util.ResourceBundle;

public class BibliotecaApp {

  ResourceBundle bibliotecaBundle;

  BibliotecaApp() {
    this.bibliotecaBundle = ResourceBundle.getBundle("BibliotecaApp");
  }

  public static void main(String[] args) {
    BibliotecaApp app = new BibliotecaApp();
    Writer writer = new ConsoleWriter();
    Reader reader = new ConsoleReader();
    writer.print(app.greet());
    MongoDB dbInstance = MongoDB.instance();
    Library library = new Library(dbInstance);
    UserManagement userManagement = new UserManagement(dbInstance);

    LoginMenu menu = new LoginMenu(reader, writer, library, userManagement);
    while (true) {
      menu.start();
    }
  }

  String greet() {
    return this.bibliotecaBundle.getString("Greeting");
  }

}