package com.twu.biblioteca;

public interface Reader {
  String getMenuOption(String pattern, String message);

  int getItemID();

  String getLibraryID();

  String getPassword();
}
