package com.twu.biblioteca;

//models a book containing user Id and item id

public class LedgerEntry {
  private final String libraryID;
  private final String itemName;

  public LedgerEntry(String libraryID, String itemName) {

    this.libraryID = libraryID;
    this.itemName = itemName;
  }

  public String details() {
    StringBuilder detail = new StringBuilder();
    detail.append("User ").append(libraryID);
    detail.append(" has checked out ").append(itemName).append("\n");
    return detail.toString();
  }
}
