package com.twu.biblioteca;

import java.util.ResourceBundle;

import static com.twu.entities.LibraryItem.Category;

public class LibraryResult {


  private final TransactionType type;
  private final Status status;
  private final Category category;

  public LibraryResult(TransactionType type, Status status, Category category) {

    this.type = type;
    this.status = status;
    this.category = category;
  }

  public String detail() {
    String aCategory = category.toString().toLowerCase();
    String aTransactionType = type.value();
    String aStatusValue = status.value();
    return type.getMessage(aCategory + aTransactionType + aStatusValue);
  }

  enum TransactionType {
    CHECKOUT("LibraryCheckOut", "CheckOut"), RETURN("LibraryReturn", "Return");

    private final String propertiesFile;
    private final String value;

    TransactionType(String propertiesFile, String value) {
      this.propertiesFile = propertiesFile;
      this.value = value;
    }

    String getMessage(String key) {
      return ResourceBundle.getBundle(propertiesFile).getString(key);
    }

    String value() {
      return value;
    }
  }

  enum Status {
    SUCCESS("Success"), FAILURE("Failure");

    private final String value;

    Status(String value) {
      this.value = value;
    }

    String value() {
      return value;
    }
  }
}
