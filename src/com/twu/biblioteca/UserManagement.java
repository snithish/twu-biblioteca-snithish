package com.twu.biblioteca;

import com.twu.entities.User;
import org.mongodb.morphia.query.Query;

//models Authenticator for library

public class UserManagement {
  private MongoDB instance;

  public UserManagement(MongoDB dbInstance) {
    instance = dbInstance;
  }

  public User login(String memberID, String password) throws AuthenticationFailedException {
    User user = getUserBasedOnCriteria(memberID);
    if (user == null) {
      throw new AuthenticationFailedException("Wrong Login credentials!");
    }
    if (user.authenticate(memberID, password)) {
      return user;
    }
    throw new AuthenticationFailedException("Wrong Login credentials!");
  }

  private User getUserBasedOnCriteria(String memberID) {
    Query<User> query = instance.getDatabase().createQuery(User.class);
    query.filter("_id", memberID);
    User user = query.get();
    return user;
  }

  public User getUser(String memberID) throws UserNotFoundException {
    User user = getUserBasedOnCriteria(memberID);
    if (user == null) {
      throw new UserNotFoundException("User not found!");
    }
    return user;
  }
}
