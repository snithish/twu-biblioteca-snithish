package com.twu.biblioteca;

import com.twu.entities.User;

import java.util.List;

public interface UserDataSourceAble {
  List<User> listOfUsers();
}
