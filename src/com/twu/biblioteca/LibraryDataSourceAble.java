package com.twu.biblioteca;

import com.twu.entities.Book;
import com.twu.entities.Movie;

import java.util.List;

public interface LibraryDataSourceAble {
  List<Book> listOfBooks();

  List<Movie> listOfMovies();
}
