package com.twu.biblioteca;

public class AuthenticationFailedException extends Exception {
  AuthenticationFailedException(String message) {
    super(message);
  }
}
