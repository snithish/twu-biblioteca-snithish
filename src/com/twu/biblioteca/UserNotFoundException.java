package com.twu.biblioteca;

public class UserNotFoundException extends Exception {
  UserNotFoundException(String message) {
    super(message);
  }
}
