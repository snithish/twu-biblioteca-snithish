package com.twu.biblioteca;

import com.twu.entities.InventoryItem;
import com.twu.entities.LibraryItem;
import com.twu.entities.User;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.twu.biblioteca.Library.Status.AVAILABLE;
import static com.twu.biblioteca.Library.Status.CHECKED_OUT;
import static com.twu.biblioteca.LibraryResult.TransactionType.CHECKOUT;
import static com.twu.biblioteca.LibraryResult.TransactionType.RETURN;
import static com.twu.entities.LibraryItem.Category;

//models a building containing borrowable items

public class Library {
  private static final LibraryResult.Status SUCCESS = LibraryResult.Status.SUCCESS;
  private static final LibraryResult.Status FAILURE = LibraryResult.Status.FAILURE;
  private MongoDB instance;

  public Library(MongoDB dbInstance) {
    instance = dbInstance;
  }

  public List<LibraryItem> listItems(Category category) {
    Datastore datastore = instance.getDatabase();
    List<InventoryItem> inventoryItems = filterOnStatus(datastore, AVAILABLE);
    return Collections.unmodifiableList(filterOnCategory(category, inventoryItems));
  }

  public LibraryResult checkOutItem(int uid, Category category, User user) {

    Datastore datastore = instance.getDatabase();
    LibraryItem libraryItem = getItem(uid, category, datastore);
    if (libraryItem != null) {
      Query<InventoryItem> query = criteriaForTransaction(datastore, libraryItem, AVAILABLE);
      InventoryItem item = query.get();
      if (item != null) {
        item.checkOut(user);
        UpdateOperations<InventoryItem> update = updateInventory(user, datastore, CHECKED_OUT);
        datastore.update(query.get(), update);
        return new LibraryResult(CHECKOUT, SUCCESS, category);
      }
    }
    return new LibraryResult(CHECKOUT, FAILURE, category);
  }

  public LibraryResult returnItem(int uid, Category category) {
    Datastore datastore = instance.getDatabase();
    LibraryItem libraryItem = getItem(uid, category, datastore);
    if (libraryItem != null) {
      Query<InventoryItem> query = criteriaForTransaction(datastore, libraryItem, CHECKED_OUT);
      InventoryItem item = query.get();
      if (item != null) {
        item.returnItem();
        UpdateOperations<InventoryItem> update = updateInventory(null, datastore, AVAILABLE);
        datastore.update(query.get(), update);
        return new LibraryResult(RETURN, SUCCESS, category);

      }
    }
    return new LibraryResult(RETURN, FAILURE, category);
  }

  public List<LedgerEntry> checkoutLedger(Category category) {
    List<LedgerEntry> entries = new ArrayList<>();
    Datastore datastore = instance.getDatabase();
    List<InventoryItem> checkedOutInventories = filterOnStatus(datastore, CHECKED_OUT);
    for (InventoryItem filteredItem : checkedOutInventories) {
      if (filteredItem.isOfCategory(category)) {
        String userID = filteredItem.userMemberID();
        String itemName = filteredItem.item().name();
        LedgerEntry entry = new LedgerEntry(userID, itemName);
        entries.add(entry);
      }
    }
    return Collections.unmodifiableList(entries);
  }

  private List<LibraryItem> filterOnCategory(Category category, List<InventoryItem> inventoryItems) {
    List<LibraryItem> items = new ArrayList<>();
    for (InventoryItem inventoryItem : inventoryItems) {
      if (inventoryItem.isOfCategory(category)) {
        items.add(inventoryItem.item());
      }
    }
    return items;
  }

  private List<InventoryItem> filterOnStatus(Datastore datastore, Status status) {
    Query<InventoryItem> queryOnStatus = datastore.createQuery(InventoryItem.class);
    queryOnStatus.filter("status", status);
    return queryOnStatus.asList();
  }

  private UpdateOperations<InventoryItem> updateInventory(User user, Datastore datastore, Status status) {
    UpdateOperations<InventoryItem> update;
    update = datastore.createUpdateOperations(InventoryItem.class);
    update.set("status", status);
    if (user != null) {
      update.set("user", user);
    }
    return update;
  }

  private Query<InventoryItem> criteriaForTransaction(Datastore datastore, LibraryItem libraryItem, Status status) {
    Query<InventoryItem> query = datastore.createQuery(InventoryItem.class);
    query.filter("item", libraryItem).filter("status", status);
    return query;
  }

  private LibraryItem getItem(int uid, Category category, Datastore datastore) {
    Query<LibraryItem> itemQuery = datastore.createQuery(LibraryItem.class);
    itemQuery.filter("_id", uid);
    itemQuery.filter("category", category);
    return itemQuery.get();
  }

  public enum Status {
    CHECKED_OUT, AVAILABLE
  }

}
