package com.twu.menus;

import com.twu.biblioteca.Library;
import com.twu.biblioteca.Reader;
import com.twu.biblioteca.Writer;

import java.util.HashMap;
import java.util.Map;

public class AdminMenu {
  public static final String INPUT_PATTERN = "[A-C]{1}";
  public static final String ERROR_MESSAGE = "Valid options are between A and C only\nRe-Enter: ";
  public static final MenuOptions LOGOUT = MenuOptions.LOGOUT;
  private final Map<MenuOptions, MenuOperation> operations;
  private final Reader reader;
  private final Writer writer;
  private final Library library;

  //models options to be loaded when user is administrator

  AdminMenu(Reader reader, Writer writer, Library library) {
    this.library = library;
    operations = new HashMap<MenuOptions, MenuOperation>();
    this.reader = reader;
    this.writer = writer;
  }

  private String menu() {
    StringBuilder menu = new StringBuilder();
    menu.append("A. View CheckOut Ledger\n");
    menu.append("B. Log Out\n");
    menu.append("C. Exit\n");
    return menu.toString();
  }

  void start() {
    operationMap();
    while (true) {
      writer.demarcator();
      writer.print(menu());
      String option = reader.getMenuOption(INPUT_PATTERN, ERROR_MESSAGE);
      MenuOptions menuOption = MenuOptions.forID(option);
      if (menuOption == LOGOUT) {
        return;
      }
      MenuOperation operation = operations.get(menuOption);
      String result = operation.execute(library);
      writer.print(result);
    }
  }

  private void operationMap() {
    operations.put(MenuOptions.VIEW_CHECKOUT_LEDGER, new MenuOperation.CheckOutLedger());
    operations.put(MenuOptions.EXIT, LoginMenu.EXIT);
  }

  private enum MenuOptions {
    VIEW_CHECKOUT_LEDGER("A"), LOGOUT("B"), EXIT("C");

    private final String optionShown;

    MenuOptions(String optionShown) {
      this.optionShown = optionShown;
    }

    public static MenuOptions forID(String optionEntered) {
      for (MenuOptions menuOption : values()) {
        if (menuOption.optionShown.equals(optionEntered)) {
          return menuOption;
        }
      }
      return null;
    }

  }

}
