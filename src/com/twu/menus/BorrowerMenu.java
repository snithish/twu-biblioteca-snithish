package com.twu.menus;

import com.twu.biblioteca.Library;
import com.twu.biblioteca.Reader;
import com.twu.biblioteca.Writer;
import com.twu.entities.User;

import java.util.HashMap;
import java.util.Map;

import static com.twu.entities.LibraryItem.Category.BOOK;
import static com.twu.entities.LibraryItem.Category.MOVIE;
import static com.twu.menus.MenuOperation.*;

//models options to be loaded when user is a customer

public class BorrowerMenu {

  private static final String PATTERN = "[A-I]{1}";
  private static final String ERROR_MESSAGE = "Valid options are between A and I only\nRe-Enter: ";
  private static final MenuOptions LOGOUT = MenuOptions.LOGOUT;
  private final Reader reader;
  private final Writer writer;
  private final Library library;
  private final User user;
  Map<MenuOptions, MenuOperation> operations;

  BorrowerMenu(Reader reader, Writer writer, Library library, User user) {
    this.reader = reader;
    this.writer = writer;
    this.library = library;
    this.user = user;
    operations = new HashMap<MenuOptions, MenuOperation>();
  }

  private void operationMap() {
    CheckOutItem checkOutBook = new CheckOutItem(reader, BOOK, user);
    ReturnItem returnBook = new ReturnItem(reader, BOOK);
    CheckOutItem checkOutMovie = new CheckOutItem(reader, MOVIE, user);
    ReturnItem returnMovie = new ReturnItem(reader, MOVIE);
    operations.put(MenuOptions.LIST_BOOKS, LoginMenu.LIST_BOOKS);
    operations.put(MenuOptions.CHECKOUT_BOOK, checkOutBook);
    operations.put(MenuOptions.RETURN_BOOK, returnBook);
    operations.put(MenuOptions.LIST_MOVIES, LoginMenu.LIST_MOVIES);
    operations.put(MenuOptions.CHECKOUT_MOVIE, checkOutMovie);
    operations.put(MenuOptions.RETURN_MOVIE, returnMovie);
    operations.put(MenuOptions.DISPLAY_USER_INFO, new UserDetails(user));
    operations.put(MenuOptions.EXIT, LoginMenu.EXIT);
  }

  public void start() {
    operationMap();
    while (true) {
      writer.demarcator();
      writer.print(menu());
      String option = reader.getMenuOption(PATTERN, ERROR_MESSAGE);
      MenuOptions menuOption = MenuOptions.forID(option);
      if (menuOption == LOGOUT) {
        return;
      }
      MenuOperation operation = operations.get(menuOption);
      String result = operation.execute(library);
      writer.print(result);
    }
  }

  private String menu() {
    StringBuilder menu = new StringBuilder();
    menu.append("Main LoginMenu\n");
    menu.append("A. List Books\n");
    menu.append("B. Checkout Book\n");
    menu.append("C. Return Book\n");
    menu.append("D. List Movies\n");
    menu.append("E. Checkout Movie\n");
    menu.append("F. Return Movie\n");
    menu.append("G. View my profile\n");
    menu.append("H. Log Out\n");
    menu.append("I. Quit\n");
    return menu.toString();
  }

  private enum MenuOptions {
    LIST_BOOKS("A"), CHECKOUT_BOOK("B"), RETURN_BOOK("C"), LIST_MOVIES("D"), CHECKOUT_MOVIE("E"), RETURN_MOVIE("F"), DISPLAY_USER_INFO("G"), LOGOUT("H"), EXIT("I");

    private final String optionShown;

    MenuOptions(String optionShown) {
      this.optionShown = optionShown;
    }

    public static MenuOptions forID(String optionEntered) {
      for (MenuOptions menuOption : values()) {
        if (menuOption.optionShown.equals(optionEntered)) {
          return menuOption;
        }
      }
      return null;
    }
  }
}
