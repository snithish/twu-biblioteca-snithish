package com.twu.menus;

import com.twu.biblioteca.*;
import com.twu.entities.LibraryItem;
import com.twu.entities.User;

import java.util.List;
import java.util.ResourceBundle;

import static com.twu.entities.LibraryItem.Category;
import static com.twu.entities.LibraryItem.Category.BOOK;

interface MenuOperation {
  MenuOperation EXIT = new MenuOperation() {
    @Override
    public String execute(Library library) {
      System.exit(0);
      return null;
    }
  };

  String execute(Library library);

  class ListItems implements MenuOperation {
    private final Category category;
    private final ResourceBundle bundle;

    ListItems(Category category) {
      this.bundle = ResourceBundle.getBundle("TableHeading");
      this.category = category;
    }

    @Override
    public String execute(Library library) {
      List<LibraryItem> items = library.listItems(category);
      StringBuilder result = new StringBuilder();
      String keyPart = category.toString().toLowerCase();
      String headingFormat = bundle.getString(keyPart + "Format");
      String headingData = bundle.getString(keyPart + "Heading");
      String[] split = headingData.split(",");
      String heading = String.format(headingFormat, split);
      result.append(heading);
      for (LibraryItem item : items) {
        result.append(item.details());
      }
      return result.toString();
    }
  }

  class CheckOutItem implements MenuOperation {

    private final Reader input;
    private final Category category;
    private final User user;

    CheckOutItem(Reader input, Category category, User user) {
      this.input = input;
      this.category = category;
      this.user = user;
    }

    @Override
    public String execute(Library library) {
      int id = input.getItemID();
      if (id == -1) {
        return "\n";
      }
      return library.checkOutItem(id, category, user).detail();
    }
  }

  class ReturnItem implements MenuOperation {
    private final Reader input;
    private final Category category;

    ReturnItem(Reader input, Category category) {
      this.input = input;
      this.category = category;
    }

    @Override
    public String execute(Library library) {
      int id = input.getItemID();
      if (id == -1) {
        return "\n";
      }
      return library.returnItem(id, category).detail();
    }
  }

  class LogIn implements MenuOperation {
    private final UserManagement userManagement;
    private final Reader reader;

    LogIn(UserManagement userManagement, Reader reader) {
      this.userManagement = userManagement;
      this.reader = reader;
    }

    @Override
    public String execute(Library library) {
      String libraryID = reader.getLibraryID();
      String password = reader.getPassword();
      User user;
      try {
        user = userManagement.login(libraryID, password);
      } catch (AuthenticationFailedException e) {
        return "Authentication Failed";
      }
      return user.memberID();
    }
  }

  class UserDetails implements MenuOperation {
    private final User user;

    UserDetails(User user) {
      this.user = user;
    }

    @Override
    public String execute(Library library) {
      return user.details();
    }
  }

  class CheckOutLedger implements MenuOperation {

    @Override
    public String execute(Library library) {
      List<LedgerEntry> entries = library.checkoutLedger(BOOK);
      StringBuilder result = new StringBuilder();
      for (LedgerEntry entry : entries) {
        result.append(entry.details());
      }
      return result.toString();
    }
  }
}
