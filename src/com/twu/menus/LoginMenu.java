package com.twu.menus;

import com.twu.biblioteca.*;
import com.twu.entities.User;

import java.util.HashMap;
import java.util.Map;

import static com.twu.entities.LibraryItem.Category.BOOK;
import static com.twu.entities.LibraryItem.Category.MOVIE;
import static com.twu.menus.MenuOperation.ListItems;
import static com.twu.menus.MenuOperation.LogIn;

//models options to be loaded when app starts

public class LoginMenu {
  public static final MenuOptions LOGIN = MenuOptions.LOGOUT;
  static final ListItems LIST_BOOKS = new ListItems(BOOK);
  static final ListItems LIST_MOVIES = new ListItems(MOVIE);
  static final MenuOperation EXIT = MenuOperation.EXIT;
  private static final String PATTERN = "[A-D]{1}";
  private static final String ERROR_MESSAGE = "Valid options are between A and D only\nRe-Enter: ";
  Map<MenuOptions, MenuOperation> operations;
  Writer writer;
  Reader reader;
  private Library library;
  private UserManagement userManagement;

  public LoginMenu(Reader reader, Writer writer, Library library, UserManagement userManagement) {
    this.library = library;
    this.userManagement = userManagement;
    this.writer = writer;
    this.reader = reader;
    operations = new HashMap<MenuOptions, MenuOperation>();
  }

  public void start() {

    operationMap();
    writer.demarcator();
    User user;
    try {
      user = loginMenuDisplayAndGetLoggedInUser();
    } catch (UserNotFoundException e) {
      writer.print("Something failed! That is all I know!");
      return;
    }
    if (!user.isAdmin()) {
      BorrowerMenu borrowerMenu = new BorrowerMenu(reader, writer, library, user);
      borrowerMenu.start();
      return;
    }
    if (user.isAdmin()) {
      AdminMenu adminMenu = new AdminMenu(reader, writer, library);
      adminMenu.start();
      return;
    }
  }

  private User loginMenuDisplayAndGetLoggedInUser() throws UserNotFoundException {
    while (true) {
      writer.print(menu());
      String option = reader.getMenuOption(PATTERN, ERROR_MESSAGE);
      MenuOptions loginMenuOption = MenuOptions.forID(option.trim());
      MenuOperation operation = operations.get(loginMenuOption);
      String result = operation.execute(library);
      if (loginMenuOption == LOGIN) {
        if (!result.equals("Authentication Failed")) {
          return userManagement.getUser(result);
        }
      }
      writer.print(result + "\n");
    }
  }

  private void operationMap() {
    LogIn login = new LogIn(this.userManagement, reader);
    operations.put(MenuOptions.LIST_BOOKS, LIST_BOOKS);
    operations.put(MenuOptions.LIST_MOVIES, LIST_MOVIES);
    operations.put(MenuOptions.LOGOUT, login);
    operations.put(MenuOptions.EXIT, EXIT);
  }

  private String menu() {
    StringBuilder menu = new StringBuilder();
    menu.append("A. List Books\n");
    menu.append("B. List Movies\n");
    menu.append("C. Log In\n");
    menu.append("D. Exit\n");
    return menu.toString();
  }

  private enum MenuOptions {
    LIST_BOOKS("A"), LIST_MOVIES("B"), LOGOUT("C"), EXIT("D");
    private final String optionShown;

    MenuOptions(String optionShown) {
      this.optionShown = optionShown;
    }

    public static MenuOptions forID(String optionEntered) {
      for (MenuOptions menuOption : values()) {
        if (menuOption.optionShown.equals(optionEntered)) {
          return menuOption;
        }
      }
      return null;
    }
  }
}
