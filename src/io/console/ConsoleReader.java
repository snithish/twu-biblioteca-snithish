package io.console;

import com.twu.biblioteca.Reader;

import java.util.Scanner;

public class ConsoleReader implements Reader {
  public int getItemID() {
    ConsoleWriter consoleWriter = new ConsoleWriter();
    consoleWriter.print("Enter ID of Item (Type 'back' to main menu): ");
    Scanner in = new Scanner(System.in);
    String input;
    while (!in.hasNext("[0-9]+")) {
      String invalidItem = in.nextLine();
      if (invalidItem.equalsIgnoreCase("back")) {
        return -1;
      }
      consoleWriter.print("Nah! ID contains value between 0 and 9 only!\nRe-Enter");
      in.nextLine();
    }
    input = in.next();
    int result = Integer.parseInt(input);
    return result;
  }

  @Override
  public String getLibraryID() {
    ConsoleWriter consoleWriter = new ConsoleWriter();
    consoleWriter.print("Enter library ID: ");
    Scanner in = new Scanner(System.in);
    String input;
    while (!in.hasNext("[A-Za-z0-9]{3}-[A-Za-z0-9]{4}")) {
      consoleWriter.print("Nah! Library ID is of format xxx-xxxx!\nRe-Enter");
      in.nextLine();
    }
    input = in.next();
    return input;
  }

  @Override
  public String getPassword() {
    ConsoleWriter consoleWriter = new ConsoleWriter();
    consoleWriter.print("Enter password: ");
    Scanner in = new Scanner(System.in);
    String input;
    input = in.nextLine();
    return input;
  }

  public String getMenuOption(String pattern, String message) {
    ConsoleWriter consoleWriter = new ConsoleWriter();
    consoleWriter.print("Enter option: ");
    Scanner in = new Scanner(System.in);
    String input;
    while (!in.hasNext(pattern)) {
      consoleWriter.print(message);
      in.nextLine();
    }
    input = in.next();
    return input;
  }
}
