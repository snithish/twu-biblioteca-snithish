package io.console;

import com.twu.biblioteca.Writer;

public class ConsoleWriter implements Writer {
  public void print(String message) {
    System.out.print(message);
  }

  public void demarcator() {
    System.out.println("\n===========================================================");
  }
}
